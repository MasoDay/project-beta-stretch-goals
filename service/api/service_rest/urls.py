from django.urls import path
from service_rest.views import (
    api_list_appts,
    api_show_appt,
    api_appt_history,
    api_list_techs,
    api_show_tech
)


urlpatterns = [
    path("service/", api_list_appts, name="api_list_appts"),
    path("service/<int:id>/", api_show_appt, name="api_show_appt"),
    path("service/history/<str:vin>/", api_appt_history, name="api_appt_history"),
    path("technicians/", api_list_techs, name="api_list_techs"),
    path("technicians/<int:id>/", api_show_tech, name= "api_show_tech"),
]
