from django.contrib import admin
from service_rest.models import Technician, ServiceAppointment

# Register your models here.

@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    pass

@admin.register(ServiceAppointment)
class ServiceAppointmentAdmin(admin.ModelAdmin):
    pass
