from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return f"{self.vin}"

    def get_url(self):
        return reverse("list_or_create_sale", kwargs={"vin": self.vin})


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=4, unique=True)

    def __str__(self):
        return f"{self.last_name}, {self.first_name}"

    class Meta:
        ordering = (
            "last_name",
            "first_name",
        )

    def get_url(self):
        return reverse("list_or_create_salesperson", kwargs={"id": self.id})


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    street_address = models.CharField(max_length=200)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=40)
    phone_number = models.CharField(max_length=12)
    email = models.EmailField(max_length=150)

    def __str__(self):
        return f"{self.last_name}, {self.first_name}"

    def get_url(self):
        return reverse("list_or_create_customer", kwargs={"id": self.id})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.PROTECT,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sale",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sale",
        on_delete=models.PROTECT,
    )
    sale_price = models.PositiveIntegerField()

    def __str__(self):
        return f"{self.automobile.vin}"

    def get_url(self):
        return reverse("list_or_create_sales", kwargs={"id": self.id})
