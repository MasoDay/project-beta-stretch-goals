from common.json import ModelEncoder
import json

from .models import AutomobileVO, Sale, Salesperson, Customer


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "color",
        "year",
        "model",
    ]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_number",
    ]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
    ]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "street_address",
        "city",
        "state",
        "phone_number",
        "email",
    ]


class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "customer",
        "salesperson",
    ]

    def get_extra_data(self, o):
        return  {"automobile": o.automobile.vin,
                 "customer": json.dumps(o.customer.id, default=str),
                 "salesperson": o.salesperson.id}


class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "customer",
        "sale_price",
        "salesperson",
    ]
    encoder = {
        "salesperson": SalespersonEncoder(),
        "customer": CustomerDetailEncoder(),
        "automobile": AutomobileVOEncoder(),
    }

    def get_extra_data(self, o):
        return  {"automobile": o.automobile.vin}
