import React, { useState, useEffect } from 'react';

function ServiceHistory () {
    const [appts, setAppts] = useState([])
    const [search, setSearch] = useState('');
    const [results, setResults] = useState([])

    const handleInputChange = (e) => {
        setSearch(e.target.value);
    };

    const getData = async () => {
        let response = await fetch('http://localhost:8080/api/service/history/${id}')
        let data = await response.json()
        setAppts(data.appts)
        setResults(data.appts)
    }

    useEffect(() => {
        getData()
    }, [])


    return (
        <>
        <form>
            <input className="form-control" value={search} onChange={handleInputChange} placeholder="Search VIN Number"/>
            <button>Search VIN</button>
        </form>
        <h1>Service History</h1>
        <table className="table table-dark table-hover table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>Owner</th>
                    <th>Date</th>
                    <th>Assigned Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {results?.map(appt => {
                    return(
                        <tr key={appt.id}>
                            <td>{appt.vin}</td>
                            <td>{appt.owner}</td>
                            <td>{appt.date}</td>
                            <td>{appt.assigned_tech.name}</td>
                            <td>{appt.reason}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </>
    )
}

export default ServiceHistory;
