import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


    const SaleList = () => {
        const [ sales, setSales ] = useState([])
        const [ formData, setFormData ] = useState([])
        const [ salespeople, setSalespeople ] = useState([])

        const getSaleData = async () => {
            const url = 'http://localhost:8090/api/sales/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setSales(data.sales);
            } else {
                console.log("There was an error displaying sales.")
            }
        }


    useEffect(() => {
        getSaleData();
    }, []
    )

    useEffect(() => {
        const loadData = async () => {
          const salespeopleUrl = 'http://localhost:8090/api/salesperson/';
          const response = await fetch(salespeopleUrl); // salespersonUrl, customerUrl);

          if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
          } else {
            console.log("There was an error loading these salespeople")
          }
        }

        loadData()

      }, [])

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Sales records</h1>

                    <div className="mb-3">
                      <select onChange={handleFormChange} value={formData.salesperson} className="form-select" required name="salesperson" id="salesperson">

                        <option value="">Choose a salesperson</option>
                        {salespeople?.map(salesperson => {
                          return (
                            <option key={salesperson.id} value={salesperson.last_name}>{salesperson.last_name}</option>
                          )
                        })}
                      </select>
                    </div>

                    <table className="table">
                        <thead>
                            <tr>
                            <th>Automobile</th>
                            <th>Customer</th>
                            <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                sales.map(sale => {
                                    return(
                                        <tr key={ sale.id }>
                                            <td>{ sale.fields.automobile }</td>
                                            <td>{ sale.fields.customer }</td>
                                            <td>{ sale.fields.sale_price }</td>
                                        </tr>
                                );
                            })}
                        </tbody>
                    </table>
                    <Link to="/sales/create/"><button className="btn btn-primary">Create new sale record</button></Link>
                </div>
            </div>
        </div>
    );
}

export default SaleList;
