




// const handleFormChange = (e) => {
//     setFormData({
//         ...formData,
//         [e.target.name]: e.target.value
//     })
// }

// const handleSubmit = async (e) => {
//     e.preventDefault();

//     const url = 'http://localhost:8090/api/customers/';
//     const fetchConfig = {
//         method: "POST",
//         body: JSON.stringify(formData),
//         headers: {
//             'Content-Type': 'application/json',
//         },
//     };

//     const response = await fetch(url, fetchConfig);
//     if (response.ok) {
//         setFormData(initialState);
//         navigate('/customers/')
//     }


// import { useEffect, useState } from 'react';
// import { useNavigate } from 'react-router-dom';

// const Form = () => {
//     const [ formData, setFormData ] = useState({
//             first_name: "",
//             last_name: "",
//             street_address: "",
//             city: "",
//             state: "",
//             phone_number: "",
//             email: ""
//         })

//     useEffect(() => {
//         const getData = async () => {
//             const response = await fetch('http://localhost:8090/api/customers/');
//             const data = response.json();

//             if (response.ok) {
//                 setCustomer(
//                     data.
//                 )
//                 const data = await response.json();
//                 setCustomer(data.customer);
//             } else {
//                 console.log("There was an error loading this new customer")
//             }
//         }

//         getData()
//     }, []);
// }


//     return <div className="row">
//         <div className="offset-3 col-6">
//             <div className="shadow p-4 mt-4">
//                 <h1>Create a new customer</h1>
//                 <form onSubmit={handleSubmit} id="create-customer-form">

//                     <div className="form-floating mb-3">
//                         <input onChange={handleFormChange} value={formData.first_name} placeholder="First Name" required type="text" name="First name" id="First name" className="form-control" />
//                         <label htmlFor="first_name">First name</label>
//                     </div>

//                     <div className="form-floating mb-3">
//                         <input onChange={handleFormChange} value={formData.last_name} placeholder="Last Name" required type="text" name="Last name" id="Last name" className="form-control" />
//                         <label htmlFor="last_name">Last name</label>
//                     </div>

//                     <div className="form-floating mb-3">
//                         <input onChange={handleFormChange} value={formData.street_address} placeholder="Street address" required type="text" name="Street address" id="Street address" className="form-control" />
//                         <label htmlFor="Street address">Street address</label>
//                     </div>

//                     <div className="form-floating mb-3">
//                         <input onChange={handleFormChange} value={formData.city} placeholder="City" required type="text" name="City" id="City" className="form-control" />
//                         <label htmlFor="City">City</label>
//                     </div>

//                     <div className="form-floating mb-3">
//                         <input onChange={handleFormChange} value={formData.state} placeholder="State" required type="text" name="State" id="State" className="form-control" />
//                         <label htmlFor="State">State</label>
//                     </div>

//                     <div className="form-floating mb-3">
//                         <input onChange={handleFormChange} value={formData.phone_number} placeholder="Phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" />
//                         <label htmlFor="phone_number">Phone number</label>
//                     </div>

//                     <div className="form-floating mb-3">
//                         <input onChange={handleFormChange} value={formData.email} placeholder="Email" required type="text" name="Email" id="Email" className="form-control" />
//                         <label htmlFor="email">Email</label>
//                     </div>

//                     <button className="btn btn-primary">Create customer</button>

//                 </form>
//             </div>
//         </div>
//     </div>
// }

// export default CustomerForm;
