import React from "react";

class ServiceAppointmentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state= {
            vin: '',
            owner: '',
            date: '',
            time: '',
            assigned_tech: '',
            assigned_tech: [],
            reason: '',
        }

        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleOwnerChange = this.handleOwnerChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleTechChange = this.handleTechChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({techs: data.techs})
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.techs;

        const apptUrl = 'http://localhost:8080/api/service/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(apptUrl, fetchConfig);
        if (response.ok) {
            const newAppt = await response.json();
            console.log(newAppt);

            this.setState({
                vin: '',
                owner: '',
                date: '',
                time: '',
                reason: '',
                assigned_tech: '',
            });
            window.location.reload();

        }
    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({vin: value})
    }

    handleOwnerChange(event) {
        const value = event.target.value;
        this.setState({owner: value})
    }

    handleDateChange(event) {
        const value = event.target.value;
        this.setState({date: value})
    }

    handleTimeChange(event) {
        const value = event.target.value;
        this.setState({time: value})
    }

    handleTechChange = (event) => {
        const value = event.target.value;
        this.setState({assigned_tech: value})
    }

    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({reason: value})
    }

    render () {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an appointment</h1>
                    <form onSubmit={this.handleSubmit} id="create-automobile-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleVinChange} value={this.state.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">VIN</label>
                    </div>

                    <div className="form-floating mb-3">
                      <input onChange={this.handleOwnerChange} value={this.state.owner} placeholder="Owner" required type="text" name="owner" id="owner" className="form-control" />
                      <label htmlFor="owner">Owner</label>
                    </div>

                    <div className="form-floating mb-3">
                      <input onChange={this.handleDateChange} value={this.state.date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                      <label htmlFor="date">Date</label>
                    </div>

                    <div className="form-floating mb-3">
                      <input onChange={this.handleTimeChange} value={this.state.time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                      <label htmlFor="time">Time</label>
                    </div>

                    <div className="form-floating mb-3">
                      <input onChange={this.handleReasonChange} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                      <label htmlFor="reason">Reason</label>
                    </div>

                    <div className="mb-3">
                      <select onChange={this.handleTechChange} value={this.state.assigned_tech} required id="assigned_tech"  name="assigned_tech" className="form-select" >
                      <option value="">Choose a technician</option>
                          {this.state.techs?.map(tech => {
                              return (
                                  <option key = {tech.id} value={tech.id}>
                                      {tech.tech_name}
                                  </option>
                              )
                          })};
                      </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
        );
    }
}

export default ServiceAppointmentForm;
