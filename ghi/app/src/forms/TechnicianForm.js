import React from "react";

class TechnicianForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tech_name: '',
            emp_number: ''
        }
        this.handleTechNameChange = this.handleTechNameChange.bind(this);
        this.handleEmpNumberChange = this.handleEmpNumberChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTech = await response.json();
            console.log(newTech);

            this.setState({
                name: '',
                emp_number: '',
            });
            window.location.reload();
        }
    }

    handleTechNameChange(event) {
        const value = event.target.value;
        this.setState({tech_name: value})
    }

    handleEmpNumberChange(event) {
        const value = event.target.value;
        this.setState({emp_number: value})
    }

    render () {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create a Technician</h1>
                  <form onSubmit={this.handleSubmit} id="create-technician-form">
                    <div className="form-floating mb-3">
                      <input onChange={this.handleTechNameChange} value={this.state.name} placeholder="Tech Name" required type="text" name="tech_name" id="tech_name" className="form-control" />
                      <label htmlFor="tech_name">Tech Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleEmpNumberChange} value={this.state.employeeNumber} placeholder="Employee Number" required type="emp_number" name="emp_number" id="emp_number" className="form-control" />
                      <label htmlFor="emp_number">Employee Number</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
        );
    }
}

export default TechnicianForm;
