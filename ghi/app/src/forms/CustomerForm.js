import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const initialState = {
        first_name: "",
        last_name: "",
        street_address: "",
        city: "",
        state: "",
        phone_number: "",
        email: ""
    };

  const CustomerForm = () => {
      let navigate = useNavigate();

      const [formData, setFormData ] = useState(initialState)

      const handleFormChange = (e) => {
        setFormData({
          ...formData,
          [e.target.name]: e.target.value
        })
      }

      const handleSubmit = async (e) => {
        e.preventDefault();

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {

          setFormData(initialState);
          navigate('/sales/customers-list/')
        }
      }

    return ( <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new customer</h1>
                <form onSubmit={handleSubmit} id="create-customer-form">

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.first_name} placeholder="First Name" required type="text" name="first_name" id="First name" className="form-control" />
                        <label htmlFor="first_name">First name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.last_name} placeholder="Last Name" required type="text" name="last_name" id="Last name" className="form-control" />
                        <label htmlFor="last_name">Last name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.street_address} placeholder="Street address" required type="text" name="street_address" id="Street address" className="form-control" />
                        <label htmlFor="Street address">Street address</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.city} placeholder="City" required type="text" name="city" id="City" className="form-control" />
                        <label htmlFor="City">City</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.state} placeholder="State" required type="text" name="state" id="State" className="form-control" />
                        <label htmlFor="State">State</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.phone_number} placeholder="Phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                        <label htmlFor="phone_number">Phone number</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.email} placeholder="Email" required type="text" name="email" id="Email" className="form-control" />
                        <label htmlFor="email">Email</label>
                    </div>

                    <button className="btn btn-primary">Create customer</button>

                </form>
            </div>
        </div>
    </div>
);
}

export default CustomerForm;
